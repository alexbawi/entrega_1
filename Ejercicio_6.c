//06. Write a program that asks for a value in centimeter and convert it into meter and kilometer.
#include <stdio.h>

int main ()
{
        //Usamos float para tener los valores con decimales
        float cm;
        float km;
        float m;        
        
        //Hacemos el bucle para comprobar que los valores sean siempre mayores a 0
        do 
        {
            printf ("Introduce el valor en centimetros: \n");
            scanf ("%f", &cm);
            getchar();
            if (cm < 0)
            {
                printf ("Introduce un valor que sea mayor que 0\n");
            }
        } while (cm < 0);
            
     
        m = cm / 100;
        km = m / 1000;
        
        printf ("El valor en metros es: %fm\n El valor en kilometros es: %fkm\n",m ,km );
        getchar();
        return 0;
}