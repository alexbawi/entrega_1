//14. Write a program that asks for a sequence of numbers to the user and show the mean of all of them. Process stops only when number 0 is introduced.
#include <stdio.h>

int main ()
{
    int suma = 0;
    int num;
    int media;    
    int i;
    
    printf ("Introduce numeros de uno en uno, para parar escribe el numero 0: \n");
    
    do 
    {
        scanf ("%i", &num);
        getchar();
        if (num != 0)
            {
                suma += num;
                i++;//Aquí contamos los numeros que se han ido introduciendo para poder hacer la media después
            }
    } while (num != 0);
    
    media = suma / i;
    
    printf ("La media es: %i\n", media);
    getchar();
    
    return 0;
}