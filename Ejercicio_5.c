//05. Write a program that asks for radius of a circle and find its diameter, circumference and area.
#include <stdio.h>

int main ()
{
        //Usamos float para tener los valores con decimales
        float r;
        float d;
        float c;
        float a;
        
        //Hacemos el bucle para comprobar que los valores sean siempre mayores a 0
        do
        {
            printf ("Introduce el radio: \n");
            scanf ("%f", &r);
            getchar();
            if (r <= 0)
            {
               printf ("Introduce un valor que sea mayor que 0\n");
            }
        } while (r <= 0);
        
        d = 2 * r;
        c = 3.14 * d;
        a = r * r * 3.14;
        
        printf ("El diametro es: %f\nLa circunferencia es: %f\n El area es: %f\n", d, c, a);
        getchar();
        return 0;
}