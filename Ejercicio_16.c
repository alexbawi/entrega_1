//16. Write a program that that displays the first 100 prime numbers.
#include <stdio.h>

int main ()
{
        int contador = 0;
        int div = 0;       
        int i;
        
        printf ("Los 100 primeros numeros primos son:");

        //inicializamos a 2 ya que los primos empiezan en 2
        for (int j = 2; contador < 100; j++)
        {
            div = 0;
            i = 2;
            
            while (i <= j && div == 0)
            {
                if (j%i == 0 && j==i)
                {
                   printf (" %i ", j);
                   contador++;
                }
                else if (j%i == 0)
                {
                    div = 1;
                }
               i++;
            }
             
        }
        
        getchar ();
        return 0;
}