//17. Write a program that prints five random numbers, like a Lottery.
#include <stdio.h>
#include <stdlib.h>

int main ()
{
    
    int numeros = 5;
    time_t t;
    
    srand((unsigned) time (&t));    
    printf ("Los numeros son: ");
    
    for (int i = 0; i<numeros; i++)
    {
        printf ("%d ", rand()%50);
    }
    
   getchar();
   return 0;
}