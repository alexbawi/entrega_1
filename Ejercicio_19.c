//19. Write a function to calculate power of a value. Function declaration should be: int RaiseToPower(int value, int power);
#include <stdio.h>
#include <stdlib.h>


int RaiseToPower(int value, int power)
{
    int resultado = 1;
    

    for (int i=0; i<power; i++)
    {
        resultado *= value;
    }
    
    return resultado;
}

int main ()
{

    int num;
    int potencia;
    int res;    
    
    printf ("Introduce un numero: \n");
    scanf ("%i", &num);
    getchar();
    printf ("Introduce la potencia: \n");
    scanf ("%i", &potencia);
    getchar();
    
    
    res = RaiseToPower (num, potencia);
    
    
    printf ("El resultado es: %i\n", res);
    
    
   getchar();
   return 0;
}