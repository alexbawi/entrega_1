/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"
#include <stdlib.h>

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;//0 logo 1 Titulo 2 Gemplay 3 Ending
    
    // TODO: Define required variables here..........................(0.5p)
    float transAlpha = 0;
    Color colorLogo = RED;
    int parpadeo = 0;
    
    const int velocidady = 8;
    const int ballSize = 25;
    const int maxVelocity = 15;
    const int minVelocity = 4;
    
    char *texto;
    Vector2 tamanoTexto;
   
    bool pause = false;
   
    int score1p = 0;
    int score2p = 0;
   
    Rectangle paladerecha;
   
    paladerecha.width = 20;
    paladerecha.height = 100;    
    
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
   
   
    Rectangle palaizquierda;
   
    palaizquierda.width = 20;
    palaizquierda.height = 100;    
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
   
   
    Vector2 ball;    
   
    Vector2 ballVelocity;
    
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    int playerSpeedY;
    
    Rectangle enemy;
    int enemySpeedY;
    
    Vector2 ballPosition;
    Vector2 ballSpeed;
    int ballRadius;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter;
    
    int framesCounter;          // General pourpose frames counter
    
    Rectangle lifeP1, lifeP2;
    
    
    //int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
     InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
   
    Font fontTtf = LoadFontEx("Resources/Stranger back in the Night.ttf", 32, 0, 250);
   
    // Init Audio Device
    InitAudioDevice();      // Initialize audio device
 
    // Load sound
    Sound fxWav = LoadSound("Resources/bounce.wav");         // Load WAV audio file
    Sound fxGoal = LoadSound("Resources/goal.wav");         // Load WAV audio file
 
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    void initGameplay(){
                pause = false;
                score1p = 0;
                score2p = 0;
                ball.x = screenWidth/2;
                ball.y = screenHeight/2;
                ballVelocity.x = minVelocity;
                ballVelocity.y = minVelocity;
                playerLife = 3;
                enemyLife = 3;
                secondsCounter = 99;
    }
    
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        //TODO Borrar pantalla para limpiar todo
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                transAlpha  = 0;
                while (transAlpha < 1){
                    BeginDrawing();
                    transAlpha += 0.01;
                     DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(),BLACK);
                     DrawText("Juego Creado por Alex Barceló Wiemeyer", GetScreenWidth()/2 - MeasureText("Juego Creado por Alex Barceló Wiemeyer", 20)/2, GetScreenHeight()/2 - 50, 20,  Fade (colorLogo, transAlpha));
                    EndDrawing();
                }
                
                double start = GetTime();
                
                while(GetTime() - start < 2){
                     BeginDrawing();
                     EndDrawing();
                     
                }
                
                transAlpha  = 1;
                while (transAlpha > 0){
                    BeginDrawing();
                    transAlpha -= 0.01;
                     DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(),BLACK);
                     DrawText("Juego Creado por Alex Barceló Wiemeyer", GetScreenWidth()/2 - MeasureText("Juego Creado por Alex Barceló Wiemeyer", 20)/2, GetScreenHeight()/2 - 50, 20,  Fade (colorLogo, transAlpha));
                    EndDrawing();
                }
                
                screen = TITLE;
                    
                
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                 // TODO: Title animation logic.......................(0.5p)
                 transAlpha  = 0;
                while (transAlpha < 1){
                    BeginDrawing();
                    transAlpha += 0.01;
                     DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(),BLACK);
                     DrawText("PONG", GetScreenWidth()/2 - MeasureText("PONG", 20)/2, GetScreenHeight()/2 - 50, 20,  RED);                     
                    EndDrawing();
                }         
                           
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                while (screen == TITLE){
                    if (IsKeyPressed(KEY_ENTER)){
                        break;
                    }
                    parpadeo++;
                    BeginDrawing();
                    DrawText("PRESS ENTER", GetScreenWidth()/2 - MeasureText("PRESS ENTER", 20)/2, GetScreenHeight()/2 + 50, 20,  (parpadeo/40)%2 == 0 ? BLACK : WHITE); 
                    EndDrawing();                     
                }
               screen = GAMEPLAY;
               initGameplay();
                
            } break;
            
            case ENDING:{
                BeginDrawing();
                DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(),BLACK);
                if (score1p > score2p){
                    DrawText("YOU WIN!!", GetScreenWidth()/4 - MeasureText("YOU WIN!!", 20)/2, GetScreenHeight()/2 - 50, 20, GREEN);
                    DrawText("YOU LOOSE", 3*GetScreenWidth()/4 - MeasureText("YOU LOOSE", 20)/2, GetScreenHeight()/2 - 50, 20, RED);
                }
                
                else{
                    DrawText("YOU LOOSE!!", GetScreenWidth()/4 - MeasureText("YOU LOOSE", 20)/2, GetScreenHeight()/2 - 50, 20, RED);
                    DrawText("YOU WIN!!", 3*GetScreenWidth()/4 - MeasureText("YOU WIN!!", 20)/2, GetScreenHeight()/2 - 50, 20, GREEN);
                }
                
                DrawText("PRESS R TO PLAY AGAIN", GetScreenWidth()/2 - MeasureText("PRESS R TO PLAY AGAIN", 20)/2, GetScreenHeight()/2 + 100, 20, WHITE); 
                DrawText("PRESS ESC TO EXIT", GetScreenWidth()/2 - MeasureText("PRESS ESC TO EXIT", 20)/2, GetScreenHeight()/2 + 200, 20, WHITE); 

                
                EndDrawing();   
                
                if (IsKeyPressed(KEY_R)){                                      
                    screen = GAMEPLAY;
                     initGameplay();
                    
                }
                 else if(IsKeyPressed(KEY_ESCAPE)){
                     exit(0);
                 }
                
                
            }break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
                
                // TODO: Player movement logic.......................(0.2p)
                
                // TODO: Enemy movement logic (IA)...................(1p)
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
               
                
                if(!pause){
                    if (IsKeyDown(KEY_Q)){
                      palaizquierda.y -= velocidady;
                    }
                   
                    if (IsKeyDown(KEY_A)){
                      palaizquierda.y += velocidady;
                    }
                   
                    if (IsKeyDown(KEY_UP)){
                      paladerecha.y -= velocidady;
                    }
                   
                    if (IsKeyDown(KEY_DOWN)){
                      paladerecha.y += velocidady;
                    }
                }
                
                if (IsKeyPressed(KEY_P)){
                  pause = !pause;
                }
               
                //Consulto Limites
                if(palaizquierda.y<0){
                    palaizquierda.y = 0;
                }
               
                if(palaizquierda.y > (screenHeight - palaizquierda.height)){
                    palaizquierda.y = screenHeight - palaizquierda.height;
                }
               
                if(paladerecha.y<0){
                    paladerecha.y = 0;
                }
               
                if(paladerecha.y > (screenHeight - paladerecha.height)){
                    paladerecha.y = screenHeight - paladerecha.height;
                }
               
                //Gestionamos el movimiento de la Bola
                if(!pause){
                    ball.x += ballVelocity.x;
                    ball.y += ballVelocity.y;
                }
               
                //Aqui marcamos gol
                if(ball.x > screenWidth - ballSize){
                    //Marca la pala izquierda
                    PlaySound(fxGoal);
                    score1p++;
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    ballVelocity.x = -minVelocity;
                    ballVelocity.y = minVelocity;
                   
                }else if(ball.x < ballSize){
                    //Marca la pala derecha
                    PlaySound(fxGoal);
                    score2p++;
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    ballVelocity.x = minVelocity;
                    ballVelocity.y = minVelocity;
                }
                
                //Tres goles y se gana
                if (score1p >= 3){
                   
                    screen = ENDING;
                }
                
                else if (score2p >= 3) {            
          
                  screen = ENDING;
                }
               
                if((ball.y > screenHeight - ballSize) || (ball.y < ballSize) ){
                    ballVelocity.y *=-1;
                    PlaySound(fxWav);
                }
         
         
                if(CheckCollisionCircleRec(ball, ballSize, paladerecha)){
                    if(ballVelocity.x>0){
                        PlaySound(fxWav);
                        if(abs(ballVelocity.x)<maxVelocity){                    
                            ballVelocity.x *=-1.5;
                            ballVelocity.y *= 1.5;
                        }else{
                            ballVelocity.x *=-1;
                        }
                    }
                }
               
                if(CheckCollisionCircleRec(ball, ballSize, palaizquierda)){
                    if(ballVelocity.x<0){
                        PlaySound(fxWav);
                        if(abs(ballVelocity.x)<maxVelocity){                    
                            ballVelocity.x *=-1.5;
                            ballVelocity.y *= 1.5;
                        }else{
                            ballVelocity.x *=-1;
                        }
                    }
                }
                
                BeginDrawing();
         
                    ClearBackground(BLACK);
                   
                    // Pinto Pala Derecha
                    DrawRectangleRec(paladerecha, BLUE);
                   
                    // Pinto Pala Izquierda
                    DrawRectangleRec(palaizquierda, BLUE);
                   
                    // Pintamos la pelota
                    DrawCircleV(ball, ballSize, BLUE);  
                    
                    
                   
                    // Texto ejemplo
                   
                   texto = FormatText("SCORE 1P: %d",score1p);
                   
                    DrawTextEx(fontTtf, texto, (Vector2){0,10}, 40, 0, PINK);
         
                    texto = FormatText("SCORE 2P: %d",score2p);
                    tamanoTexto = MeasureTextEx(fontTtf, texto, 40, 0);
                    /*DrawRectangle (0,10, 200,20, WHITE);
                    DrawRectangle (0,10, 200-200*(score2p/3),20, BLUE);
                    
                    DrawRectangle (screenWidth-200,10, 200,20, WHITE);
                    DrawRectangle (screenWidth-200,10, 200-200*(score1p/3),20, BLUE);*/
                
                   
                    DrawTextEx(fontTtf, texto, (Vector2){screenWidth - tamanoTexto.x,10}, 40, 0, PINK);
                    //DrawText(FormatText("SCORE 1P: %d",score1p), 10, 10, 40, RED);
                    //DrawText(FormatText("SCORE 2P: %d",score2p), screenWidth - 300, 10, 40, RED);
                   
                    if(pause){
                        DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                        DrawText("Press p to continue", screenWidth/2 - MeasureText("Press p to continue", 40)/2 , screenHeight/2, 40, PINK);
                    }
                   
                EndDrawing();
                //------------------------------------
                    
            }
        }
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    
    
    return 0;  
    
}



