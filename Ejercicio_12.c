//12. Write a program that asks for 10 numbers to the user and show the sum and product of all of them. Use while loop.
#include <stdio.h>

int main ()
{
        //Usamos int porque no se usarán decimales
        int sum = 0;
        int producto = 1;
        int i = 0;
        int num;
        
        while (i<10)
        {
            printf ("Introduce el valor %i de la secuencia de 10 numeros: \n", i+1);        
            scanf ("%i", &num);
            getchar ();
            producto *= num;
            sum += num;
            i++;
        }
                
        printf ("La suma total de la secuencia es: %d\n El producto total de la secuencia es: %d\n",sum, prdt);
        getchar();
        return 0;
}