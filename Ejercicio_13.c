//13. Write a program that asks for a sequence of numbers to the user and show the sum of all of them. Process stops when a negative number is introduced.
#include <stdio.h>

int main ()
{
     
        int sum = 0;
        int num;
        
        //Iniciamos el bucle, se va arealizar mientras el numero introducuido sera mayor a 0
        while (num >= 0)
        {
            printf ("Escibe el valor que quieres sumar, si es negativo acabara el proceso: \n");        
            scanf ("%i", &num);
            getchar ();
            
            if (num >= 0)
            {
                    sum += num;
            }
            
        }
        
        
        printf ("La suma total es: %d\n",sum);
        getchar();
        return 0;
}