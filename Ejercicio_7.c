//07. Write a program that asks for a temperature in Celsius(°C) and convert it into Fahrenheit(°F).
#include <stdio.h>

int main ()
{
        
        //Usamos float para tener los valores con decimales
        float c;
        float f;      
        
        printf ("Introduce el valor en Celsius: \n");
        scanf ("%f", &c);
        getchar();
        
        //Hacemos la conversion de C a F 
        f = c*9/5+32;        
        
        printf ("El valor en Fahrenheit es: %fF\n",f);
        getchar();
        return 0;
}