//18. Write a function to check if an integer is negative. Function declaration should be: int IsNegative(int num);
#include <stdio.h>
#include <stdlib.h>

int isNegative(int num)
{
    if (num >= 0)
    {
        return 0;
    }
    else 
    {
        return 1;
    }
}

int main ()
{
   
    int num;
    int posneg;
    
    
    printf ("Introduce un numero y te diré si es positivo o negativo: \n");
    scanf ("%i", &num);
     getchar();
    
   
    posneg = isNegative (num);
    
    //1 nos dará negativo, 0 positivo
    if (posneg == 1)
    {
         printf ("Es negativo\n");
    }
    else 
    {
        printf ("Es positivo\n");
    }
    
   getchar();
   return 0;
}