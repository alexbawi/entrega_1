//15. Write a program that asks for 1 number to the user and show its binary equivalent value. You can show the binary in inverted order.
#include <stdio.h>

int main ()
{
     
        int divisor;
        int resto;
        int num;
        
        do{
        printf ("Introduce el numero que quieres pasar a binario: \n");        
        scanf ("%i", &num);
        getchar ();
        
        if (num < 0)
            {
                printf ("El valor es menor que 0, introduce un valor valido\n");
            }
        
        } while (num < 0);
        
        printf ("El numero en binario se mostrara en orden inverso: ");
        
        while (num > 0)
        {
            resto = num % 2;
            num /= 2;
            printf ("%d", resto);            
        }
        printf ("\n"); 
        
        getchar ();
        return 0;
}