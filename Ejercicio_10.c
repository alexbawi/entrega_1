//10. Write a program that asks for 2 angles of a triangle and find the third angle.
#include <stdio.h>

int main()  
{  
    //Valores de los angulos
    int ang1;
    int ang2;
    int ang3;
  
    
    printf("Escribe dos numeros con un espacio en medio: ");  
    scanf("%d %d", &ang1, &ang2); 
    getchar();    
  
     ang3 = 180 - (ang1 + ang2);  
  
    printf("El tercer angulo es:  %d\n", ang3);  
     getchar();  
   
  
    return 0;  
}  