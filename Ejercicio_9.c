//09. Write a program that asks for seconds and convert it into minutes, hours and days
#include <stdio.h>

int main ()
{
       //Usamos float para tener los valores con decimales
        float s;
        float m;
        float h;
        float d;
        
       
        do
        {
        printf ("Introduce los segundos: \n");
        scanf ("%f", &s);
        getchar();
        
        //Hacemos el bucle para comprobar que los valores sean siempre mayores a 0
        if (s<0)
        {
            printf ("El valor introducido es menor a 0, introduce un valor valido\n");
        }
        
        }while (s<0);
        
        m = s/60;
        h = m/60;
        d = h/24;
        
        printf ("El valor en minutos es: %fm\n El valor en horas es: %fh\n El valor en dias es: %fd\n",s,m,d);
        getchar();
        return 0;
}